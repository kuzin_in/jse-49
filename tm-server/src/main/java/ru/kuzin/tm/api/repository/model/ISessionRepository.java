package ru.kuzin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    boolean existsById(@NotNull String id);

    void remove(@NotNull Session model);

}