package ru.kuzin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.repository.dto.ISessionDtoRepository;
import ru.kuzin.tm.dto.model.SessionDTO;

import javax.persistence.EntityManager;

public final class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements ISessionDtoRepository {

    public SessionDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    protected Class<SessionDTO> getEntityClass() {
        return SessionDTO.class;
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return entityManager.find(getEntityClass(), id) != null;
    }

    @Override
    public void remove(@NotNull final SessionDTO model) {
        entityManager.remove(model);
    }

}