package ru.kuzin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.IReceiverService;

import javax.jms.*;

public final class ReceiverService implements IReceiverService {

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ReceiverService(@NotNull ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createQueue(QUEUE);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}